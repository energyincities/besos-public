import sys
import os
import calendar
import traceback
import pdb

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import traceback
from ast import literal_eval

# declare variables
DATA_PATH = os.path.abspath(os.path.join(os.path.dirname('../../../../'), 'Data'))
TIME_CONSTANT_PATH = os.path.join(DATA_PATH, 'time_constant_analysis')
RAW_DATA_PATH = os.path.join(DATA_PATH, 'raw_data')
PLOT_PATH = os.path.join(DATA_PATH, 'plots')


def get_balance_points(filename, balance_point_df):
    bp = balance_point_df[balance_point_df['filename'].str.contains(filename)]
    return bp


def get_energy_balance(energy_balance_path, filename):
    ef = os.path.join(energy_balance_path, filename)
    ef_df = pd.read_csv(ef)
    ef_df['tau'] = 1 / ef_df['tau']
    return ef_df


def get_decay(decay_path, filename):
    dc = os.path.join(decay_path, filename)
    dc_df = pd.read_csv(dc)
    dc_df['tau'] = dc_df['tau']
    return dc_df


def get_filenames(directories):
    filenames = set()
    for directory in directories:
        for d in os.listdir(directory):
            filenames.add(d)
    return list(filenames)


def check_skip(df):
    skip = (len(df) == 0 or (df['start_time'] == 0).any())
    return skip


def aggregate(energy_balance_path, decay_path, balance_path):
    summary_dfs = []
    filenames = get_filenames([decay_path, energy_balance_path])
    balance_point_df = get_balance_point_df(balance_path)
    for filename in filenames:
        row = {}
        row['filename'] = filename
        try:
            # Summarize house energy_balance
            house_energy_balance = get_energy_balance(energy_balance_path, filename)
            # divide by 12 to convert to hours since timesteps are every 5 minutes
            house_energy_balance['tau'] = house_energy_balance['tau'].divide(12)
            row['energy_balance_RK_mean']   = house_energy_balance['RK'].mean()
            row['energy_balance_RK_median'] = house_energy_balance['RK'].median()
            row['energy_balance_RK_std']    = house_energy_balance['RK'].std()
            row['energy_balance_RC_mean']   = house_energy_balance['tau'].mean()
            row['energy_balance_RC_median'] = house_energy_balance['tau'].median()
            row['energy_balance_RC_std']    = house_energy_balance['tau'].std()
            row['energy_balance_cost']      = house_energy_balance['cost'].mean()
            row['energy_balance_amnt']      = len(house_energy_balance)
        except Exception as e: pass
        try:
            # Summarize house decay
            house_decay = get_decay(decay_path, filename)
            house_decay['tau'] = house_decay['tau'].divide(12)
            if np.isinf(house_decay['tau_variance'].mean()):
                raise Exception
            row['decay_amnt'] = len(house_decay)
            row['decay_RC_mean']    = house_decay['tau'].mean()
            row['decay_RC_median']  = house_decay['tau'].median()
            row['decay_RC_std']     = house_decay['tau'].std()
            row['decay_RC_cost']    = house_decay['tau_variance'].mean() 
        except: pass
        try:
            house_balance_point = get_balance_points(filename, balance_point_df)
            row['balance_RK']       = house_balance_point['slope'].values[0]
            row['balance_stderr']   = house_balance_point['stderr'].values[0]
            row['balance_rvalue']   = house_balance_point['r_value'].values[0]
            row['balance_p_value']  = house_balance_point['p_value'].values[0]
        except: pass
        row_df = pd.DataFrame(row, index=[0])        
        summary_dfs.append(row_df)
    summary_df = pd.concat(summary_dfs, ignore_index=True, sort=True)
    return summary_df


def get_balance_point_df(balance_point_file):
    balance_point_df = pd.read_csv(balance_point_file)
    balance_point_df['slope'] = -1 / balance_point_df['slope']
    return balance_point_df


def run_aggregation(results_directory, outpath):
    decay_curve_path = os.path.join(results_directory, 'decay_curves')
    energy_balance_path = os.path.join(results_directory, 'energy_balance')
    balance_point_file =  os.path.join(results_directory, 'bp.csv')
    aggregated = aggregate(
        energy_balance_path, decay_curve_path, balance_point_file)
    aggregated.to_csv(outpath)
    print('Done!')
