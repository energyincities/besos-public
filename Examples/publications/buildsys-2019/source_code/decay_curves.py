import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
import traceback
from source_code.select_data_utils import select_dc_intervals
from source_code.utils import create_new_data_file


MODEL_TYPE = "decay_curves"


def result_to_dict(start_time, end_time, T_in_mean, T_out_mean, result):
    tau = result[0][0]
    T0 = result[0][1]
    perr = np.sqrt(np.diag(result[1]))
    tau_variance = perr[0]
    T0_variance = perr[1]
    
    result = {
            "start_time": start_time,
            "end_time": end_time,
            "T_in_mean": T_in_mean,
            "T_out_mean": T_out_mean,
            "tau": tau,
            "T0": T0,
            "tau_variance": tau_variance,
            "T0_variance": T0_variance
    }
    return result


def decay_curve(t, tau, theta0):
    return theta0*np.exp(-t/tau)


def fit_decay_curve(t_in, t_out_mean, initial_guess=[100.0, 10.0]):
    xdata = np.arange(0, (len(t_in)*5) - 1, 5.0)
    # TODO think about stationarity of outside temperatures
    ydata = t_in - t_out_mean
    result = curve_fit(decay_curve, xdata, ydata, p0=initial_guess) #was 10
    return result


def analyze_building_decay_curves(months, hours, duration, filename, building_df, decay_curve_params, initial_guess):
    cols = ('Thermostat_Temperature', 'T_out', 'auxHeat1')
    time_intervals = select_dc_intervals(months, hours, decay_curve_params, building_df, cols)
    results = []
    
    for time_interval in time_intervals:
        try:
            df_interval = building_df[time_interval[0]:time_interval[-1]]
            t_in, t_out = df_interval[cols[0]], df_interval[cols[1]]
            t_in_mean, t_out_mean = t_in.mean(), t_out.mean()
            result = fit_decay_curve(t_in, t_out_mean, initial_guess)
            
            result_dict = result_to_dict(time_interval[0], time_interval[-1], t_in_mean, t_out_mean, result)
            results.append(result_dict)
            
        except Exception as e:
            print(e)
            print(traceback.print_exc())
            pass
    
    results_df = pd.DataFrame(results)
    filepath = create_new_data_file(MODEL_TYPE, filename)
    results_df.to_csv(filepath, index=False)
    return results_df
